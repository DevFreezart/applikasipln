/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package applikasi.pln;

/**
 *
 * @author freezart
 */
public class Pembayaran {
    
    private int idTagihan;
    
    private int idPelanggan;
    
    private double tarifPerKwh;
    
    private double totalMeter;
    
    private double totalBayar;

    public int getIdTagihan() {
        return idTagihan;
    }

    public void setIdTagihan(int idTagihan) {
        this.idTagihan = idTagihan;
    }

    public int getIdPelanggan() {
        return idPelanggan;
    }

    public void setIdPelanggan(int idPelanggan) {
        this.idPelanggan = idPelanggan;
    }

    public double getTarifPerKwh() {
        return tarifPerKwh;
    }

    public void setTarifPerKwh(double tarifPerKwh) {
        this.tarifPerKwh = tarifPerKwh;
    }

    public double getTotalMeter() {
        return totalMeter;
    }

    public void setTotalMeter(double totalMeter) {
        this.totalMeter = totalMeter;
    }

    public double getTotalBayar() {
        return totalBayar;
    }

    public void setTotalBayar(double totalBayar) {
        this.totalBayar = totalBayar;
    }
    
    
}
