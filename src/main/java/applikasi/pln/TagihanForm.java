/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applikasi.pln;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author freezart
 */
public class TagihanForm extends javax.swing.JInternalFrame {

    /**
     * Creates new form TagihanForm
     */
    public TagihanForm() {
        initComponents();

        getDataTable();
        loadPelangganItem();
        cmbPenggunaan.setEnabled(false);
    }

    private void getDataTable() {
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Nama Pelanggan");
        model.addColumn("Bulan");
        model.addColumn("Tahun");
        model.addColumn("Jumlah Meter");
        model.addColumn("Status");

        try {

            String query = "select "
                    + "p.nama_pelanggan, "
                    + "tg.bulan, "
                    + "tg.tahun, "
                    + "tg.jumlah_meter, "
                    + "tg.status "
                    + "from tagihan tg "
                    + "inner join pelanggan p "
                    + "on tg.id_pelanggan = p.id_pelanggan ";

            String idPelanggan = cmbPelanggan.getSelectedItem().toString();
            if (idPelanggan != null && !idPelanggan.trim().isEmpty()) {
                query += "where id_langgan=" + getIdCmb(idPelanggan);
            }

            String idPenggunaan = cmbPenggunaan.getSelectedItem().toString();
            if (idPenggunaan != null && !idPenggunaan.trim().isEmpty()) {
                if (idPelanggan != null && !idPelanggan.trim().isEmpty()) {
                    query += " and id_penggunaan=" + getIdCmb(idPenggunaan);
                } else {
                    query += "where id_penggunaan=" + getIdCmb(idPenggunaan);
                }
            }

            Connection con = Koneksi.koneksiDB();
            PreparedStatement prep = con.prepareStatement(query);
            ResultSet res = prep.executeQuery();

            while (res.next()) {
                model.addRow(new Object[]{res.getObject("daya"), res.getObject("tarifperkwh")});
            }

            tblTagihan.setModel(model);
        } catch (SQLException ex) {
            Logger.getLogger(TarifForm.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private int getIdCmb(String value) {
        return Integer.valueOf(value.split("::")[0]);
    }
    
    private void loadPelangganItem() {
        try {
            
            String query = "select * from pelanggan";
            
            Connection con = Koneksi.koneksiDB();
            PreparedStatement prep = con.prepareStatement(query);
            ResultSet res = prep.executeQuery();
            
            while (res.next()) {
                String item = res.getObject("id_pelanggan") +"::"+ res.getString("nama_pelaggan");
                cmbPelanggan.addItem(item);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(TarifForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }  
    
    private void loadPenggunaanItem() {
        cmbPelanggan.removeAllItems();
        try {
            
            String query = "select * from penggunaan where id_pelanggan=?";
            
            Connection con = Koneksi.koneksiDB();
            PreparedStatement prep = con.prepareStatement(query);
            prep.setInt(1, getIdCmb(cmbPelanggan.getSelectedItem().toString()));
            ResultSet res = prep.executeQuery();
            
            while (res.next()) {
                String item = res.getObject("id_penggunaan") +"::"+ res.getString("bulan") +"::"+ res.getObject("tahun");
                cmbPenggunaan.addItem(item);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(TarifForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cmbPelanggan = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        cmbPenggunaan = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTagihan = new javax.swing.JTable();

        setClosable(true);
        setTitle("Tagihan");

        jLabel1.setText("Pelanggan");

        cmbPelanggan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbPelangganActionPerformed(evt);
            }
        });

        jLabel2.setText("Penggunaan");

        tblTagihan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblTagihan);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbPenggunaan, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbPelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 697, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmbPelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cmbPenggunaan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmbPelangganActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbPelangganActionPerformed
        
        cmbPenggunaan.setEnabled(true);
        loadPenggunaanItem();
    }//GEN-LAST:event_cmbPelangganActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cmbPelanggan;
    private javax.swing.JComboBox cmbPenggunaan;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblTagihan;
    // End of variables declaration//GEN-END:variables
}
